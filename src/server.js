import { app as config } from 'config';
import { app } from 'app/app';

app.listen(config.port).on('listening', () => console.log(`Started @ ${config.port}`));
