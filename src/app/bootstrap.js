import { init as sentry } from 'modules/sentry/init';
import { init as mongo } from 'modules/mongo/init';

export const bootstrap = async () => {
  try {
      await sentry();
      await mongo();
  } catch (Exception) {
      throw Exception;
  }
};