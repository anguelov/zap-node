import express from 'express';
const router = express.Router();

router.use('/public', express.static('public'), (req, res) => res.end());

router.get('/', (req, res, next) => {
    res.render('index');
});

export default router;

