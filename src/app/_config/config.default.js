import OS from 'os';
import path from 'path';

export const app = {
    port: 3030,
    dns: 'localhost',
    bind: '0.0.0.0',
    ip: '127.0.0.1',
    hostname: OS.hostname(),
    base: path.join(__dirname, '..', '..'),
    env: process.env.config_env || process.env.NODE_ENV || 'LOCAL',
    _debug: false,
    envs: {
        LOCAL: 'LOCAL',
        DEVELOPMENT: 'DEVELOPMENT',
        STAGING: 'STAGING',
        PRODUCTION: 'PRODUCTION'
    },
    get debug() {
        return this._debug;
    },
    set debug(mode) {
        if (typeof mode === 'boolean' && (mode !== this._debug)) {
            this._debug = mode;
            //event.emit('config.debug.change', this._debug);
        }
    },
    get publicURL() {
        return `http://${this.dns}:${this.port}`;
    },
};

export const DB = {
    mongo: {
        protocol: 'mongodb',
        host: 'localhost',
        port: '27017',
        schema: 'helios',
        connectionRetryTimeOut: 3000,
        get url() {
            return `${DB.mongo.protocol}://${DB.mongo.host}:${DB.mongo.port}/${DB.mongo.project}`;
        }
    },
    mongoose: {
        autoIndex: true
    },
};

export const SENTRY = {
    project: '47',
    key: '505b254f06374f9ba7e8fd737b2852e8:b13f8a3a57c14600b70aba107905396a',
    public_key: '505b254f06374f9ba7e8fd737b2852e8',
    host: '10.151.52.125',
    get url() {
        return `http://${SENTRY.key}@${SENTRY.host}/${SENTRY.project}`;
    },
    config: {
        environment: 'config.env',
        logger: 'default',
        name: OS.hostname(),
        tags:{},
        extra: {},
        autoBreadcrumbs: {
            'console': true,
            'http': true,
        },
        maxBreadcrumbs: 100,
        SENTRY_RELEASE: undefined,
        captureUnhandledRejections: true,
        sendTimeout: 5
    },
    captureMessageOptions: {
        level: 'info'
    }
};