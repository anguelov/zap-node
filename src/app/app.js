import express from 'express';
import Raven from 'raven';
import { bootstrap } from 'app/bootstrap';
import router from 'services/router';
export const app = express();

bootstrap().then(() => {
    app.set('view engine', 'ejs');
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use(Raven.requestHandler());
    app.use(router);
    app.use(Raven.errorHandler());
});
