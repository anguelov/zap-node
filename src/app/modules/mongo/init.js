import mongoose from 'mongoose';
import raven from 'raven';
import { app, DB } from 'config';

export const init = async function init() {
    const mongoLogger = function mongooseLogger(collection, method, query, doc, options) {
        const queryString = JSON.stringify(query, null, '   ');
        console.log('Mongoose', JSON.parse(JSON.stringify({collection, method, doc, query: queryString, options})));
    };

    mongoose.Promise = global.Promise;
    mongoose.set('debug', app.debug ? mongoLogger : app.debug);
    mongoose.Error.messages.general.default = '`{VALUE}` não é válido';
    mongoose.Error.messages.general.required = 'Campo `{PATH}` é de preenchimento obrigatório.';

    return (async function initializeConnection() {
        try {
            const { connection } = await mongoose.connect(DB.mongo.url, DB.mongoose);
            const { host, port, name } = connection;

            console.log('Successfull mongo cliente connection', { host, port, name });
            mongoose.connection.on('error', err => { throw err; });
            return true;
        } catch (Exception) {
            raven.captureException(Exception);
            console.log(`${Exception.name}: ${Exception.message}`);
            console.log('Retry mongo connection...');
            setTimeout(() => {
                initializeConnection();
            }, DB.mongo.connectionRetryTimeOut);
            return false;
        }
    })();
};
