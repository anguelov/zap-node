import crypto from 'crypto';

export const generateRandomBytes = function generateRandomBytes(length = 50) {
    return crypto.randomBytes(length).toString('hex');
};

export const generateRandomCode = function generateRandomCode(length = 50) {
    return parseInt(crypto.randomBytes(length).toString('hex'), 10); // TODO: ter atenção ao lenght da chave gerada
};
