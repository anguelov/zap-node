module.exports = {
    entry: {
        app: './src/components/app.js',
    },
    output: {
        path: require('path').join(__dirname, '/public/js'),
        filename: '[name].js',
    },
    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    compact: true
                },
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }

        ]
    }
};